import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { LoadingService } from '../services/loading.service';
import { TentangComponent } from '../tentang/tentang.component';
import { ModalController, NavController } from '@ionic/angular';
import * as firebase from 'firebase/app';
import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed } from '@capacitor/core';

const { PushNotifications, Modals } = Plugins;

@Component({
  selector: 'app-beranda',
  templateUrl: './beranda.page.html',
  styleUrls: ['./beranda.page.scss'],
})
export class BerandaPage {

  uid:any;
  avatar:any;
  data:any;
  kata:any;
  timestamp: any;
  status:boolean;

  constructor(
    public fsAuth:AngularFireAuth, 
    private router:Router, 
    private db:AngularFirestore,
    public storage: AngularFireStorage,
    public loadingService: LoadingService,
    public modalCtrl: ModalController,
  ) { 
      this.fsAuth.auth.onAuthStateChanged(user=>{
      this.uid=user.uid;
      this.getUserData(user.uid);
      this.getPesananData(user.uid);
      this.getPesananDataDiproses(user.uid);
      this.getQuotes();
    });
  }

  ngOnInit() {
    this.timestamp = firebase.firestore.FieldValue.serverTimestamp();
    // Register with Apple / Google to receive push via APNS/FCM
    PushNotifications.register();

    // On succcess, we should be able to receive notifications
    PushNotifications.addListener('registration',
      (token: PushNotificationToken) => {
        this.saveToken(token.value);
      }
    );

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError',
      (error: any) => {
        console.log('Error on registration: ' + JSON.stringify(error));
      }
    );

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener('pushNotificationReceived',
      (notification: PushNotification) => {
        var audio1 = new Audio('assets/audio/pristine.mp3');
        audio1.play();
        // alert('Push received: ' + JSON.stringify(notification));
        console.log('Push received: ', notification);

        let alertRet = Modals.alert({
          title: notification.title,
          message: notification.body
        });
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener('pushNotificationActionPerformed',
      (notification: PushNotificationActionPerformed) => {
        this.router.navigateByUrl('/tabs/pesanan');
      }
    );
  }

  saveToken(token) {
    const data = {
      token,
      userId: this.uid,
      role: 'runner',
      datetime: this.timestamp
    };
    this.db.collection('devices').doc(token).set(data).then(res=>{
      
    });
  }

  async tentang() {
    const modal = await this.modalCtrl.create({
      component: TentangComponent,
    });

    return await modal.present();
  }

  userData:any={};
  getUserData(uid)
  {
      this.db.collection('runner').doc(uid).valueChanges().subscribe(res=>{
      this.userData=res;
      this.getImage(this.userData.avatar);
      this.status = this.userData.status;
      this.data=res;        
    })
  }

  images:any={};
  getImage(ref)
  {
      if (ref != null && this.images[ref]==undefined)
      {
        this.images[ref] = '';
        this.storage.storage.ref().child(ref).getDownloadURL().then(res => {
          this.avatar = res;
        });
      }
  }

  jumlahSelesai: any = [];
  getPesananData(uid) {
    this.db.collection('pesanan', ref => {
      return ref.where('runner', '==', uid).where('status', '==', 'selesai').orderBy('waktu', 'desc');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.jumlahSelesai = res.length;
    })
  }

  jumlahDiproses: any = [];
  getPesananDataDiproses(uid) {
    this.db.collection('pesanan', ref => {
      return ref.where('runner', '==', uid).where('status', '==', 'diproses').orderBy('waktu', 'desc');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.jumlahDiproses = res.length;
    })
  }

  getQuotes() {
    this.db.collection('quotes').valueChanges({ idField: 'id' }).subscribe(res => {
      this.kata = res;
    })
  }

  orderan() {
    this.router.navigate(['/tabs/aktivitas']);
  }

  //status akun

  loadingAction: any = {};
  aktivasi() {
    var dt = {
      status : this.status
    };
    
    this.db.collection('runner').doc(this.uid).update(dt).then(res => {
      this.loadingAction = {};
    });
    this.loadingService.present({
      message: 'Tunggu yaa..',
      duration: 1000
    });
  }

}
