import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AktivitasPage } from './aktivitas.page';

describe('AktivitasPage', () => {
  let component: AktivitasPage;
  let fixture: ComponentFixture<AktivitasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AktivitasPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AktivitasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
