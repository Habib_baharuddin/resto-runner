import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlertController, ModalController, NavController } from '@ionic/angular';
import { PesananAntarComponent } from './pesanan-antar/pesanan-antar.component';
import { Router } from '@angular/router';
import { DetailPesananPage } from '../pesanan/detail-pesanan/detail-pesanan.page';
import { AngularFireStorage } from '@angular/fire/storage';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-aktivitas',
  templateUrl: 'aktivitas.page.html',
  styleUrls: ['aktivitas.page.scss']
})
export class AktivitasPage {

   uid: any;
  dataReturned:any;
  infoOrder:any={};
  orderData:any={};
  runner:any={};
  riwayat:any = 'diproses';

  constructor(
    public fsAuth: AngularFireAuth,
    public modalCtrl : ModalController,
    public db: AngularFirestore,
    public storage: AngularFireStorage,
    private router: Router,
    public alertController: AlertController,
  ) {
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.riwayat = 'diproses';
      this.getPesananData();
      this.getPesananDataDiproses();
    });
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getPesananData();
      this.getPesananDataDiproses();
      event.target.complete();
    }, 2000);
  }

  swipeEvent(e) {
    if(e.direction == '2'){
      this.riwayat = 'selesai';
    }
    else if(e.direction == '4'){
      this.riwayat = 'diproses';
    }
  }

  pesananData: any = [];
  getPesananData() {
    this.db.collection('pesanan', ref => {
      return ref.where('runner', '==', this.uid).where('status', '==', 'selesai').orderBy('waktu', 'desc');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.pesananData = res;
      this.parseData(res);
    })
  }

  pesananDataDiproses: any = [];
  getPesananDataDiproses() {
    this.db.collection('pesanan', ref => {
      return ref.where('runner', '==', this.uid).where('status', '==', 'diproses').orderBy('waktu', 'desc');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.pesananDataDiproses = res;
      this.parseData(res);
      this.parseDataDiproses(res);
    })
  }

  parseDataDiproses(data)
  {
    for(var i=0; i<data.length; i++)
    {
      this.getImage(data[i].infoOrder.gambar_produk);
      this.getUserDataDiproses(data[i].uid);
    }
  }

  userDataDiproses:any={};
  getUserDataDiproses(uid)
  {
    this.db.collection('members').doc(uid).valueChanges().subscribe(res=>{
      this.userDataDiproses[uid]=res;
    })
  }


  parseData(data)
  {
    for(var i=0; i<data.length; i++)
    {
      this.getImage(data[i].infoOrder.gambar_produk);
      this.getUserData(data[i].uid);
    }
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }

  userData:any={};
  getUserData(uid)
  {
    this.db.collection('members').doc(uid).valueChanges().subscribe(res=>{
      this.userData[uid]=res;
    })
  }

  async pesananAntar(data){
    const modal = await this.modalCtrl.create({
      component: PesananAntarComponent,
      componentProps: {
        "orderData": data,
        "mode":'terima',
        "pemesan":this.userData[data.uid]
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal kirim data :'+ dataReturned);
      }
    });
 
    return await modal.present();
  }

  async pesananDiproses(data){
    const modal = await this.modalCtrl.create({
      component: DetailPesananPage,
      componentProps: {
        "orderData": data,
        "mode":'terima',
        "pemesan":this.userData[data.uid]
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal kirim data :'+ dataReturned);
      }
    });
 
    return await modal.present();
  }

}
