import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PesananAntarComponent } from './pesanan-antar.component';

describe('PesananAntarComponent', () => {
  let component: PesananAntarComponent;
  let fixture: ComponentFixture<PesananAntarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesananAntarComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PesananAntarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
