import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, NavParams } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlertController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SweetAlert } from 'sweetalert/typings/core';
import { Router } from '@angular/router';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-pesanan-antar',
  templateUrl: './pesanan-antar.component.html',
  styleUrls: ['./pesanan-antar.component.scss'],
})
export class PesananAntarComponent implements OnInit {

  uid: any;
  warung: any;
  orderData:any={}; 
  infoOrder:any={};
  data:any=[];
  timesstamp:any;
  mode:any;
  pemesan:any={};
  status: any;
  id_produk: any;
  id_runner: any;

  constructor(
    public fsAuth: AngularFireAuth,
    public storage: AngularFireStorage,
    public db: AngularFirestore,
    private navParams: NavParams,
  	private modalCtrl: ModalController,
    public alertController: AlertController,
    private http: HttpClient,
    private router:Router
  ) {
  	this.fsAuth.auth.onAuthStateChanged(user=>{
      this.id_runner=user.uid;
    });
  }

  ngOnInit() {
    this.orderData = this.navParams.get('orderData');
    this.mode=this.navParams.get('mode');
    this.pemesan=this.navParams.get('pemesan');
    this.infoOrder=this.orderData.infoOrder; 
    this.uid = this.orderData.uid;
    this.warung = this.orderData.warung;
    this.id_produk = this.infoOrder.id_produk;
    this.getProduk(this.id_produk);
    this.getUserData(this.uid);
    this.getRunnerData(this.id_runner);
    this.getPesananData(this.orderData);
    this.getWarungData(this.warung);
  }

  pesananData: any=[];
  getPesananData(data) {
    var order=data.orderData;
    var obj=Object.keys(order);
    for(var i=0; i<obj.length;i++)
    {
      var dt=order[obj[i]];
      dt.id=obj[i];
      this.getImage(order[obj[i]].url);
      this.pesananData.push(dt);
    }
    this.status = data.status;
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }

  async batal() {
    const onClosedData: string = "Kembali";
    await this.modalCtrl.dismiss(onClosedData);
  }

  loading:boolean;
  selesai()
  {
    var r = confirm("Pesanan selesai ?");
    if (r == true) { 
      this.loading=true;
      var data = { status : 'selesai' }
      this.db.collection('pesanan').doc(this.orderData.id).update(data).then(res=>{
        this.loading=false;
        this.updateSaldo();
        this.kirimNotifikasi(this.orderData);
        this.komisi();
        this.komisiWarung();
        this.updateStok();
      })
    }  else {
      return;
    }
  }

  selesaitunai()
  {
    var r = confirm("Pesanan selesai ?");
    if (r == true) { 
      this.loading=true;
      var data = { status : 'selesai' }
      this.db.collection('pesanan').doc(this.orderData.id).update(data).then(res=>{
        this.loading=false;
        this.kirimNotifikasi(this.orderData);
        this.updateStok();
        this.presentAlert();
      })
    }  else {
      return;
    }
  }

  url_notif = 'https://fcm.googleapis.com/fcm/send';

  kirimNotifikasi(orderData) {
    var id_user = orderData.uid;
    this.db.collection('devices', ref => {
      return ref.where('userId', '==', id_user)
    }).valueChanges().subscribe(res => {
      this.parseDevices(res);
    })
  }

  parseDevices(res) {
    var dataDevices = res;
    var obj = Object.keys(dataDevices);
    for (var i = 0; i < obj.length; i++) {
      let data = {
        "notification" : {
          "title":"PesanAja",
          "body":"Pesanan selesai, cek sekarang!",
          "sound":"default",
          "click_action":"FCM_PLUGIN_ACTIVITY",
          "icon":"fcm_push_icon"
        },
        "to": dataDevices[obj[i]].token,
        "priority":"high",
        "restricted_package_name":""
      };

      let headers: HttpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `key=AAAApnCT334:APA91bEE2Ft48A28KZDuB5C2FF0UqpW9Es20IXIr4kjaeMfiTEibOqB1MBsVjBP_aXCZT9fLgEvuT8nePcBrWkcPTm31ItdllvRhdYqiXkTNzZ6e8nsKwzPP69pBo7FDrrdApepJshwJ`
      });

      this.http.post(this.url_notif, data, { headers }).subscribe( res => {
      });
    }
  }

  dataProduk: any ={};
  getProduk(id_produk) {
    this.db.collection('produk').doc(id_produk).valueChanges().subscribe(res => {
      this.dataProduk = res;
    })
  }

  userData: any = {};
  getUserData(uid) {
    this.db.collection('members').doc(uid).valueChanges().subscribe(res => {
      this.userData = res;
      this.parseUser(res);
    })
  }

  parseUser(res) {
    this.getImage(res.avatar);
  }

  runnerData: any = {};
  getRunnerData(uid) {
    this.db.collection('runner').doc(uid).valueChanges().subscribe(res => {
      this.runnerData = res;
    })
  }

  warungData:any={};
  getWarungData(uid)
  {
    this.db.collection('owner').doc(uid).valueChanges().subscribe(res=>{
      this.warungData=res;
      this.getImage(this.warungData.avatar);
    })
  }

  updateStok() {
    var stok = this.dataProduk.stok - this.infoOrder.jumlahItem;
    var data = { stok: stok };

    this.db.collection('produk').doc(this.id_produk).update(data).then(res => {
      
    });
  }

  komisi() {
    var saldo = this.runnerData.penghasilan + this.orderData.infoOrder.ongkir;
    var data = { penghasilan: saldo };

    this.db.collection('runner').doc(this.id_runner).update(data).then(res => {
      
    });
  }

  chat(){
    this.batal();
    this.router.navigate(['/chat/', this.orderData.id]);
  }

  komisiWarung() {
    var total_harga = this.orderData.infoOrder.harga * this.orderData.infoOrder.jumlahItem;
    var saldo = this.warungData.pendapatan + total_harga;
    var data = { pendapatan: saldo };

    this.db.collection('owner').doc(this.warung).update(data).then(res => {
      
    });
  }

  updateSaldo() {
    var saldo = this.userData.saldo - this.orderData.infoOrder.totalHarga;
    var data = { saldo: saldo };

    this.db.collection('members').doc(this.uid).update(data).then(res => {
      this.presentAlert();
    });
  }

  presentAlert() {
    swal({   
      title: "Sukses",   
      text: "Pesanan selesai.",   
      icon: "success",
      timer: 2000,   
    });
    this.batal();
  }

  hapus(rowID)
  {
    this.alertConfirm(rowID);
  }

   //confirm
   async alertConfirm(rowID) {
    const alert = await this.alertController.create({
      header: 'Hapus Data',
      message: 'Anda yakin ingin menghapus data ini secara permanen?',
      buttons: [
        {
          text: 'Tidak',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            return false;
          }
        }, {
          text: 'Ya',
          handler: () => {
            this.db.collection('pesanan').doc(rowID).delete().then(res=>{
              swal({   
                title: "Hapus Data Pesanan",   
                text: "Data pesanan berhasil dihapus.",   
                icon: "success",
                timer: 3000,
              });
              this.batal();
            })
            
          }
        }
      ]
    });
    await alert.present();
  }

}
