import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { from } from 'rxjs';
import { ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    public afAuth: AngularFireAuth, 
    private router:Router,
    public toastController:ToastController,
    private db: AngularFirestore

    ) { }

  ngOnInit() {
  }

  loading:boolean;
  login(email, password) {
    this.loading=true;
    this.afAuth.auth.signInWithEmailAndPassword(email, password).then(res=>{
      if(res.user.emailVerified) {
        this.cekAkses(res.user.uid);
        this.loading = false;
      } else {
        swal({   
          title: "Gagal",   
          text: "Email belum diverifikasi.",   
          icon: "danger",
          timer: 2000,   
        });
        this.sendEmailVerification(email);
        return false;
      }
    }).catch(error=>{
        this.loading=false;
        this.toastAlert(error.message);
    })
  }
  
  sendEmailVerification(email) {
    this.afAuth.authState.subscribe(user => {
        user.sendEmailVerification()
        .then(() => {
          swal({   
            title: "Sukses",   
            text: "Email verifikasi terkirim.",   
            icon: "success",
            timer: 2000,   
          });
          this.router.navigate(['verify-email']);
        })
    });
  }

  cekAkses(uid) {
    this.db.collection('runner').doc(uid).get().subscribe(res => {
      if (res.data() != undefined)
        this.routeAction(res.data());
      else this.noaccess();
    })
  }

  noaccess() {
    this.afAuth.auth.signOut();
    swal({   
			title: "Gagal",   
			text: "Anda tidak memiliki hak akses.",   
			icon: "danger",
			timer: 2000,   
		});
  }

  routeAction(data) {
    if (data.role == 'runner') {
      localStorage.setItem('uid', data.uid);
      this.router.navigate(['/tabs/beranda']);
    }
    else this.noaccess();
  }


  async toastAlert(msg){
    const toast=await this.toastController.create({
      header:'Login Failure',
      message:msg,
      duration:3000,
      position:'bottom'
    });
    toast.present();
  }

  public type = 'password';
  public showPass = false;
  showPassword() {
    this.showPass = !this.showPass;
    if(this.showPass){
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

}
