import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PengaturanPage } from './pengaturan.page';
import { PengaturanPageRoutingModule } from './pengaturan-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    PengaturanPageRoutingModule
  ],
  declarations: [PengaturanPage]
})
export class PengaturanPageModule {}
