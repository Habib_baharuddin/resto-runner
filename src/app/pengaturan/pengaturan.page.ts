import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { LoadingService } from '../services/loading.service';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { AlertController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-pengaturan',
  templateUrl: 'pengaturan.page.html',
  styleUrls: ['pengaturan.page.scss']
})
export class PengaturanPage implements OnInit {

  uid:any;
  avatar:any;
  data:any;

  constructor(
    public fsAuth:AngularFireAuth, 
    private router:Router, 
    private db:AngularFirestore,
    public storage: AngularFireStorage,
    public alertController: AlertController,
    public loadingService: LoadingService,
    public actionSheetController: ActionSheetController,
    ) {
      this.fsAuth.auth.onAuthStateChanged(user=>{
      this.uid=user.uid;
      this.getUserData(user.uid);
    });
  }

  ngOnInit() {
    //   this.loadingService.present({
    //   message: 'Menyiapkan Data',
    //   duration: 2500
    // });
  }

  keluar()
  {
    this.alertConfirm();
  }

  //confirm
  async alertConfirm() {
    const alert = await this.alertController.create({
      header: 'Keluar',
      message: 'Apakah Anda yakin?',
      buttons: [
        {
          text: 'Tidak',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            return false;
          }
        }, {
          text: 'Ya',
          handler: () => {
            this.fsAuth.auth.signOut().then(res=>{
              this.router.navigate(['/login']);
            })
            
          }
        }
      ]
    });
    await alert.present();
  }
  
  async ActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Foto Sampul',
      buttons: [{
        text: 'Kamera',
        icon: 'camera',
        handler: () => {
          this.takeKamera();
          console.log(this.updateAvatar);
        }
      }, {
        text: 'Buka Galeri Foto',
        icon: 'image',
        handler: () => {
          this.takeGaleri();
          console.log('Share clicked');
        }
      }, {
        text: 'Batal',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  userData:any={};
  getUserData(uid)
  {
    this.db.collection('runner').doc(uid).valueChanges().subscribe(res=>{
        this.userData=res;
        this.getImage(this.userData.avatar);
        this.data=res;       
    })
  }

  loading:boolean;
  updateData()
  {
      this.loading=true;
      this.db.collection('runner').doc(this.uid).update(this.userData).then(res=>{
          this.loading=false;
      })
  }

  async takeKamera() {
    const image = await Plugins.Camera.getPhoto({
      quality: 80,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera
    });
    this.avatar = image.dataUrl;
    this.updateAvatar(this.avatar);
  }

  async takeGaleri() {
    const image = await Plugins.Camera.getPhoto({
      quality: 80,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Photos
    });
    this.avatar = image.dataUrl;
    this.updateAvatar(this.avatar);
  }

  // async uploadAvatar() {
  //   const image = await Plugins.Camera.getPhoto({
  //     quality: 80,
  //     allowEditing: false,
  //     resultType: CameraResultType.DataUrl,
  //     source: CameraSource.Camera
  //   });
  //   this.avatar = image.dataUrl;
  //   this.updateAvatar(this.avatar);
  // }

  updateAvatar(imageData)
  {
    var filename = this.uid + '/avatar.png';
    var storageRef = this.storage.storage.ref();
    var avatarRef = storageRef.child(filename);
    avatarRef.putString(imageData, 'data_url').then(snapshot => {     
      this.updateUser(filename);
    });
  }

  updateUser(fn)
  {
    var dt={
      avatar:fn
    };
    this.db.collection('runner').doc(this.uid).update(dt).then(dat=>{
      return;
    })
  }

  images:any={};
  getImage(ref)
  {
      if (ref != null && this.images[ref]==undefined)
      {
        this.images[ref] = '';
        this.storage.storage.ref().child(ref).getDownloadURL().then(res => {
          this.avatar = res;
        });
      }
  }

}
