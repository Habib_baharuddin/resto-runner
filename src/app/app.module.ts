import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { DetailPesananPageModule } from './pesanan/detail-pesanan/detail-pesanan.module';
import { environment } from '../environments/environment';
import { PesananAntarComponent } from './aktivitas/pesanan-antar/pesanan-antar.component';
import { SyaratKetentuanComponent } from './syarat-ketentuan/syarat-ketentuan.component';
import { TentangComponent } from './tentang/tentang.component';
import { Toast } from '@ionic-native/toast/ngx';

@NgModule({
  declarations: [AppComponent, PesananAntarComponent, SyaratKetentuanComponent, TentangComponent],
  entryComponents: [PesananAntarComponent, SyaratKetentuanComponent, TentangComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    DetailPesananPageModule,
    HttpClientModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Toast,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {}
