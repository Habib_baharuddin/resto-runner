import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'pesanan',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pesanan/pesanan.module').then(m => m.PesananPageModule)
          }
        ]
      },
      {
        path: 'pengaturan',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../pengaturan/pengaturan.module').then(m => m.PengaturanPageModule)
          }
        ]
      },
      {
        path: 'aktivitas',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../aktivitas/aktivitas.module').then(m => m.AktivitasPageModule)
          }
        ]
      },
      {
        path: 'beranda',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../beranda/beranda.module').then(m => m.BerandaPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/pesanan',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/pesanan',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
