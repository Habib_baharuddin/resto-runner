import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { ModalController, NavController } from '@ionic/angular';
import { DetailPesananPage } from './detail-pesanan/detail-pesanan.page';
import { LoadingService } from '../services/loading.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { Router } from '@angular/router';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = require('sweetalert');

@Component({
  selector: 'app-pesanan',
  templateUrl: 'pesanan.page.html',
  styleUrls: ['pesanan.page.scss']
})
export class PesananPage implements OnInit {

  uid: any;
  dataReturned:any;
  infoOrder:any={};
  orderData:any={};
  runner:any={};

  constructor(
    public fsAuth: AngularFireAuth,
    public modalCtrl : ModalController,
    public db: AngularFirestore,
    public storage: AngularFireStorage,
    public loadingService: LoadingService
  ) {
    this.fsAuth.auth.onAuthStateChanged(user => {
      this.uid = user.uid;
      this.getUser(user.uid);
    });
  }

  ngOnInit() {
    this.loadingService.present({
      message: 'Menyiapkan Data',
      duration: 2000
    });
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getPesananData();
      event.target.complete();
    }, 2000);
  }

  pesananData: any = [];
  getPesananData() {
    this.db.collection('pesanan', ref => {
      return ref.where('runner', '==', '0').where('status', '==', 'diproses').orderBy('waktu', 'desc');
    }).valueChanges({ idField: 'id' }).subscribe(res => {
      this.parseData(res);
    })
  }

  parseData(data)
  {
    for(var i=0; i<data.length; i++)
    {
      if(data[i].status != 'ditolak') {
        this.pesananData.push(data[i]);
      }
      this.getImage(data[i].infoOrder.gambar_produk);
      this.getUserData(data[i].uid);
    }
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }

  userData:any={};
  getUserData(uid)
  {
    this.db.collection('members').doc(uid).valueChanges().subscribe(res=>{
      this.userData[uid]=res;
    })
  }

  dataRunner: any={};
  getUser(uid)
  {
      this.db.collection('runner').doc(uid).valueChanges().subscribe(res=>{
      this.dataRunner=res;
      if(this.dataRunner.status == true) {    
        this.getPesananData();
      } else {
        swal({   
          title: "PesanAja",   
          text: "Aktifkan akun untuk menerima order.",   
          icon: "warning",
          timer: 2000,   
        });
      }
    })
  }

  async detailPesanan (data){
    this.updateStatus(data);
    const modal = await this.modalCtrl.create({
      component: DetailPesananPage,
      componentProps: {
        "orderData": data,
        "mode":'terima',
        "pemesan":this.userData[data.uid]
      }
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
        //alert('Modal kirim data :'+ dataReturned);
        this.pesananData = [];
        this.getPesananData();
      }
    });
 
    return await modal.present();
  }

  updateStatus(data) {
    
    var dat = { onView: true };

    this.db.collection('pesanan').doc(data.id).update(dat).then(res => {
      
    })
  }

}
