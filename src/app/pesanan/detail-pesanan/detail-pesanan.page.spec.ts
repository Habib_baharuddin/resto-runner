import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailPesananPage } from './detail-pesanan.page';

describe('DetailPesananPage', () => {
  let component: DetailPesananPage;
  let fixture: ComponentFixture<DetailPesananPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPesananPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailPesananPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
