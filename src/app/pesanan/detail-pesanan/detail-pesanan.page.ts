import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, NavParams } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-detail-pesanan',
  templateUrl: './detail-pesanan.page.html',
  styleUrls: ['./detail-pesanan.page.scss'],
})
export class DetailPesananPage implements OnInit {
  uid: any;
  orderData:any={}; 
  infoOrder:any={};
  data:any=[];
  timesstamp:any;
  constructor(
    public fsAuth: AngularFireAuth,
    public db: AngularFirestore,
    private navParams: NavParams,
    public storage: AngularFireStorage,
    private router: Router,
    private modalCtrl: ModalController,
    private http: HttpClient
  )
     { 
      this.fsAuth.auth.onAuthStateChanged(user => {
        this.uid = user.uid;       
      });
     }

  mode:any;
  pemesan:any={};
  ngOnInit() {
    this.orderData = this.navParams.get('orderData');
    this.mode=this.navParams.get('mode');
    this.pemesan=this.navParams.get('pemesan');
    this.getPesananData(this.orderData);
    this.getImage(this.pemesan.avatar);
  }

  pesananData: any=[];
  getPesananData(data) {
    var order=data.orderData;
    var obj=Object.keys(order);
    for(var i=0; i<obj.length;i++)
    {
      var dt=order[obj[i]];
      dt.id=obj[i];
      this.pesananData.push(dt);
      this.getImage(order[obj[i]].url);
    }
  }

  images: any = {};
  getImage(ref) {
    if (this.images[ref] == undefined) {
      this.storage.storage.ref(ref).getDownloadURL().then(url => {
        this.images[ref] = url;
      });
    }
  }

  async batal() {
    this.updateStatus(this.orderData);
    const onClosedData: string = "Batal !";
    await this.modalCtrl.dismiss(onClosedData);
  }

  updateStatus(data) {
    
    var dat = { onView: false };

    this.db.collection('pesanan').doc(data.id).update(dat).then(res => {
      
    })
  }
  
  loading:boolean;
  terima() {   
    this.loading=true;
    var data = { runner : this.uid }
    this.db.collection('pesanan').doc(this.orderData.id).update(data).then(res=>{
      this.loading=false;
      this.kirimNotifikasi(this.orderData);
      this.batal();
      this.router.navigate(['/tabs/aktivitas']);
    })
  }

  url_notif = 'https://fcm.googleapis.com/fcm/send';

  kirimNotifikasi(orderData) {
    console.log(orderData);
    var id_user = orderData.uid;
    this.db.collection('devices', ref => {
      return ref.where('userId', '==', id_user)
    }).valueChanges().subscribe(res => {
      this.parseDevices(res);
    })
  }

  parseDevices(res) {
    var dataDevices = res;
    console.log(dataDevices);
    var obj = Object.keys(dataDevices);
    for (var i = 0; i < obj.length; i++) {
      let data = {
        "notification" : {
          "title":"PesanAja",
          "body":"Pesanan sedang diproses, cek sekarang!",
          "sound":"default",
          "click_action":"FCM_PLUGIN_ACTIVITY",
          "icon":"fcm_push_icon"
        },
        "to": dataDevices[obj[i]].token,
        "priority":"high",
        "restricted_package_name":""
      };

      let headers: HttpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `key=AAAApnCT334:APA91bEE2Ft48A28KZDuB5C2FF0UqpW9Es20IXIr4kjaeMfiTEibOqB1MBsVjBP_aXCZT9fLgEvuT8nePcBrWkcPTm31ItdllvRhdYqiXkTNzZ6e8nsKwzPP69pBo7FDrrdApepJshwJ`
      });

      this.http.post(this.url_notif, data, { headers }).subscribe( res => {
        console.log(res);
      });
    }
  }

}
