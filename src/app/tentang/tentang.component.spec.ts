import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TentangComponent } from './tentang.component';

describe('TentangComponent', () => {
  let component: TentangComponent;
  let fixture: ComponentFixture<TentangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TentangComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TentangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
